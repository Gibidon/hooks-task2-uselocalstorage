import { useEffect, useState } from 'react'

type ReturnValue = [
  value: string | null,
  {
    setItem: (value: string) => void
    removeItem: () => void
  },
]

export function useLocalStorage(key: string): ReturnValue {
  const [value, setValue] = useState(() => getDataFromLocalStorage(key))

  function setItem(newToken: string): void {
    setValue(newToken)
  }

  function removeItem(): void {
    setValue('')
  }

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value))
  }, [value])

  return [value, { setItem, removeItem }]
}

function getDataFromLocalStorage(key: string, initialState = 'init') {
  const savedData = JSON.parse(localStorage.getItem(key))

  if (savedData) return savedData
  else return initialState
}
