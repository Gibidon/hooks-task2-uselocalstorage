import { useLocalStorage } from './hooksHW/useLocalStorage'

export function App() {
  const [token, { setItem, removeItem }] = useLocalStorage('token')

  return (
    <div>
      <p className="text-lg font-bold">Твой токен: {token}</p>
      <div>
        <button onClick={() => setItem('new-token')}>Задать токен</button>
        <button onClick={() => removeItem()}>Удалить токен</button>
      </div>
    </div>
  )
}
